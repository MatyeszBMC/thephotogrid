//
//  AppDelegate.swift
//  ThePhotoGrid
//
//  Created by Matyas Varga on 2018. 09. 05..
//  Copyright © 2018. Matyas Varga. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    var tokenStore = TokenStore()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        switch url.scheme {
        case "com.demo.ThePhotoGrid":
            print("SUCCESS")
            return true
        default:
            break
        }
        return false
    }

}

