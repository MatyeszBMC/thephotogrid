//
//  TokenStore.swift
//  ThePhotoGrid
//
//  Created by Matyas Varga on 2018. 09. 05..
//  Copyright © 2018. Matyas Varga. All rights reserved.
//

import Foundation

class TokenStore {
    var oauth2AccessTokenStore = [URL: String]()
    var latestOAuth2AccessToken: String?
    
    func store(token: String, for url: URL) {
        oauth2AccessTokenStore[url] = token
        latestOAuth2AccessToken = token
    }
    
    func deleteToken(for url: URL) {
        oauth2AccessTokenStore[url] = nil
    }
    
    func token(for url: URL) -> String? {
        return oauth2AccessTokenStore[url]
    }
    
}
