//
//  WKWebViewPresenter.swift
//  ThePhotoGrid
//
//  Created by Matyas Varga on 2018. 09. 18..
//  Copyright © 2018. Matyas Varga. All rights reserved.
//

import UIKit
import WebKit

class WKWebViewPresenter {
    
    var wkWebViewController: UIViewController & WKUIDelegate
    let urlToLoad: URL
    
    public init(wkWebViewController: UIViewController & WKUIDelegate, url: URL?) throws {
        self.wkWebViewController = wkWebViewController
        guard let urlNotNil = url else {
            throw ApplicationError.urlIsNil
        }
        self.urlToLoad = urlNotNil
    }
    
    public func presentWebView(completionHandler: (Error?) -> (Void)) {
        
    }
    
    public func dismissWebView(completionHandler: (Error?) -> (Void)) {
        
    }
}
