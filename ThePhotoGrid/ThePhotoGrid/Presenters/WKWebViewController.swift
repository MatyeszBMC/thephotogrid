//
//  WKWebViewController.swift
//  ThePhotoGrid
//
//  Created by Matyas Varga on 2018. 09. 18..
//  Copyright © 2018. Matyas Varga. All rights reserved.
//

import UIKit
import WebKit

class WKWebViewController: UIViewController, WKUIDelegate, WKNavigationDelegate {
    
    var webView: WKWebView!
    var url: URL!
    
    override func loadView() {
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        webView.navigationDelegate = self
        view = webView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let urlNotNil = url {
            let myRequest = URLRequest(url: urlNotNil)
            webView.load(myRequest)
        } else {
            self.dismiss(animated: true)
        }
    }
    
    // MARK: - WKNavigationDelegate
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
        let application = UIApplication.shared
        let url = navigationAction.request.url
        let myScheme = "http://localhost/callback"
        
        if let urlNotNil = url, let scheme = urlNotNil.scheme, scheme.elementsEqual(myScheme), application.canOpenURL(urlNotNil) {
            application.open(urlNotNil, options: [:]) { result in
                result ? decisionHandler(.allow) : decisionHandler(.cancel)
            }
        }
        decisionHandler(.allow)
    }
}
