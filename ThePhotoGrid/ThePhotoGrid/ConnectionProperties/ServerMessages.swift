//
//  ServerMessages.swift
//  ThePhotoGrid
//
//  Created by Matyas Varga on 2018. 09. 05..
//  Copyright © 2018. Matyas Varga. All rights reserved.
//

// MARK: - Errors which should be thrown by the server
enum ServerError {
    case internalError //500
    case notImplemented //501
    case badGateway //502
    case serviceUnavilable //503
    case gatewayTimeout //504
}
