//
//  ConnectionDetails.swift
//  ThePhotoGrid
//
//  Created by Matyas Varga on 2018. 09. 05..
//  Copyright © 2018. Matyas Varga. All rights reserved.
//

enum ConnectionDetails {
    public static let baseAuthorizationURL = "https://api.instagram.com/oauth/authorize/"
    public static let apiURL = "https://api.instagram.com/v1/users/"
    public static let clientID = "972ae94ee59b40c1aca9d855131b6cb3"
    public static let clientSecret = "ba8ca566cf0c4792be41eab6796d8cf1"
    public static let redirectURL = "http://localhost/callback"
    public static let accessToken = "access_token"
    public static let scope = "basic+public_content"
    public static let authorizationURL = String(format: "%@?client_id=%@&redirect_uri=%@&response_type=token&scope=%@&DEBUG=True", arguments: [ConnectionDetails.baseAuthorizationURL, ConnectionDetails.clientID, ConnectionDetails.redirectURL, ConnectionDetails.scope ])
}
