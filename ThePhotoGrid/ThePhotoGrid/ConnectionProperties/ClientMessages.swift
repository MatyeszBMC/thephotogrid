//
//  ClientMessages.swift
//  ThePhotoGrid
//
//  Created by Matyas Varga on 2018. 09. 05..
//  Copyright © 2018. Matyas Varga. All rights reserved.
//

// MARK: - Errors which should be thrown by the client
enum ClientError {
    case badRequest //400
    case unauthorized //401
    case forbidden //403
    case notFound //404
    case methodNotAllowed //405
    case notAcceptable //406
    case requestTimeout //408
}

enum ApplicationError: Error {
    case urlIsNil
}
