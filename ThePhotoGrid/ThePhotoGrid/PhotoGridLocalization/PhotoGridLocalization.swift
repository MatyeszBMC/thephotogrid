//
//  PhotoGridLocalization.swift
//  ThePhotoGrid
//
//  Created by Matyas Varga on 2018. 09. 05..
//  Copyright © 2018. Matyas Varga. All rights reserved.
//
import Foundation

struct PhotoGridLocalization {
    public static let cancelButtonPressedOnLogin = NSLocalizedString("cancelButtonPressedDuringLoginKey", tableName: "LoginViewController", bundle: Bundle.main, value: "Cancel log in", comment: "Title of alertview when cancel pressed on log in screen.")
    
    public static let cancelButtonPressedOnLoginMessage = NSLocalizedString("cancelButtonPressedDuringLoginKey", tableName: "LoginViewController", bundle: Bundle.main, value: "Log in to instagram will be cancelled. Are you sure?", comment: "Message of alertview when cancel pressed on log in screen.")
    
    public static let cancelButtonLoginView = NSLocalizedString("cancelButtonAlertViewKey", tableName: "LoginViewController", bundle: Bundle.main, value: "Cancel", comment: "Title of cancel button on alert view.")
    
    public static let retryButtonLoginView = NSLocalizedString("retryButtonAlertViewKey", tableName: "LoginViewController", bundle: Bundle.main, value: "Retry", comment: "Title of retry button on alert view.")
}
