//
//  WebViewController+UIWebViewControllerDelegate.swift
//  ThePhotoGrid
//
//  Created by Matyas Varga on 2018. 09. 05..
//  Copyright © 2018. Matyas Varga. All rights reserved.
//

import Foundation
import UIKit

extension WebViewController: UIWebViewDelegate {
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        return loadCallbackURL(from: request)
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        if self.activityIndicator.isAnimating {
            self.activityIndicator.stopAnimating()
            self.activityIndicator.isHidden = true
        }
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        self.webViewDidFinishLoad(webView)
    }
    
    func loadCallbackURL(from request: URLRequest) -> Bool {
        guard let url = request.url else {
            print("The url is not valid.")
            return false
        }
        let requestURLString = url.absoluteString
        
        if requestURLString.hasPrefix(ConnectionDetails.redirectURL) {
            let range: Range<String.Index> = requestURLString.range(of: "#access_token=")!
            let oauthToken = requestURLString[range.upperBound...]
            self.tokenStore.store(token: String(oauthToken), for: url)
            if self.activityIndicator.isAnimating {
                self.activityIndicator.stopAnimating()
                self.activityIndicator.isHidden = true
            }
            self.uiWebView.stopLoading()
            self.uiWebView.removeFromSuperview()
        }
        return true
    }
}

extension WebViewController {
    func presentPhotoGridViewController(completionHandler: @escaping (Error?) -> ()) {
        
        completionHandler(nil)
    }
}

