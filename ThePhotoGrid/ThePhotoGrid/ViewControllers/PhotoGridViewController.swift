//
//  PhotoGridViewController.swift
//  ThePhotoGrid
//
//  Created by Matyas Varga on 2018. 09. 11..
//  Copyright © 2018. Matyas Varga. All rights reserved.
//

import UIKit

class PhotoGridViewController: UICollectionViewController {
    
    // MARK: - Properties
    private let reuseIdentifier = "ImageCell"
    private let sectionInsets = UIEdgeInsets(top: 50.0, left: 20.0, bottom: 50.0, right: 20.0)
    
    override func viewDidLoad() {
        //something will come here
    }
}
