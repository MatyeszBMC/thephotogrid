//
//  ImageCell.swift
//  ThePhotoGrid
//
//  Created by Matyas Varga on 2018. 09. 14..
//  Copyright © 2018. Matyas Varga. All rights reserved.
//

import UIKit

class ImageCell: UICollectionViewCell {
    
    @IBOutlet weak var cellImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
