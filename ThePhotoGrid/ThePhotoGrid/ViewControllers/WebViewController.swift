//
//  WebViewController.swift
//  ThePhotoGrid
//
//  Created by Matyas Varga on 2018. 09. 05..
//  Copyright © 2018. Matyas Varga. All rights reserved.
//

import UIKit

class WebViewController: UIViewController {
    
    @IBOutlet weak var uiWebView: UIWebView!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var tokenStore: TokenStore!
    
    override func viewDidLoad() {
        self.uiWebView.delegate = self
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelTapped))
        self.navigationItem.leftBarButtonItem = cancelButton
        
        if let tokenStore = (UIApplication.shared.delegate as? AppDelegate)?.tokenStore {
            self.tokenStore = tokenStore
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //Forward user to instagram.com
        let instagramURL = URL(string: ConnectionDetails.authorizationURL)!
        let request = URLRequest(url: instagramURL, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 300)
        self.uiWebView.loadRequest(request)
    }
    
    @objc func cancelTapped() {
        let alertController = UIAlertController(title: PhotoGridLocalization.cancelButtonPressedOnLogin, message: PhotoGridLocalization.cancelButtonPressedOnLoginMessage, preferredStyle: .alert)
        
        let cancel = UIAlertAction(title: PhotoGridLocalization.cancelButtonLoginView, style: .cancel)
        let retry = UIAlertAction(title: PhotoGridLocalization.retryButtonLoginView, style: .default)
        
        alertController.addAction(cancel)
        alertController.addAction(retry)
        self.present(alertController, animated: true)
    }
}
